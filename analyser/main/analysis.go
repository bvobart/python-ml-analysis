package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"time"

	"github.com/fatih/color"
	"gitlab.com/bvobart/python-ml-analysis/git"
	"gitlab.com/bvobart/python-ml-analysis/input"
	"gitlab.com/bvobart/python-ml-analysis/ipynb"
	"gitlab.com/bvobart/python-ml-analysis/output"
	"gitlab.com/bvobart/python-ml-analysis/pylint"
	"gitlab.com/bvobart/python-ml-analysis/utils"
	"gitlab.com/bvobart/python-ml-analysis/virtualenv"
)

const generatedReqsMarkerFile = ".requirements-generated"

type Analyser struct {
	// the project to be analysed
	Project input.Project
	// the results of the analysis
	Results output.Results

	// time at which the analysis started
	startTime time.Time
}

func NewAnalyser(project input.Project, startTime time.Time) Analyser {
	results := output.NewResults(project)
	return Analyser{Project: project, Results: results, startTime: startTime}
}

// ------------------------------------------------------------------

func (a *Analyser) Log(format string, args ...interface{}) {
	message := color.YellowString(format, args...)
	log.Println(color.YellowString("--> Project %2d - %s - %s", a.Project.ID, utils.FixedLength(a.Project.Name), message), "time:", time.Since(a.startTime))
}

func (a *Analyser) LogWarning(format string, args ...interface{}) {
	message := color.HiYellowString(format, args...)
	log.Println(color.HiYellowString("--> Project %2d - %s - WARNING! - %s", a.Project.ID, utils.FixedLength(a.Project.Name), message))
}

// ------------------------------------------------------------------

// IsCloned returns true iff the project has already been cloned
// i.e. is present on disk
func (a *Analyser) IsCloned() bool {
	repoDir := path.Join(dir, a.Project.CloneFolder)
	gitDir := path.Join(repoDir, ".git")
	return utils.DirExists(repoDir) && utils.DirExists(gitDir)
}

// CloneProject clones the project contained in the Analyser to a folder in the 'projects' directory
func (a *Analyser) CloneProject() error {
	if a.IsCloned() {
		a.Log("Already cloned!")
		return nil
	}

	a.Log("Cloning...")
	repoDir := path.Join(dir, a.Project.CloneFolder)

	err := git.Clone(a.Project.CloneURL, repoDir)
	if err != nil {
		return a.NewSetupError(fmt.Errorf("failed to clone Git repo: %w", err))
	}

	return nil
}

// EnsureRequirementsFile ensures that the default or specified requirements.txt file exists
// and generates one if it doesn't.
// Also sets analyser.Results.HasRequirementsFile and analyser.Results.CustomRequirements
// Returns the location of the requirements file relative to the project directory,
// so that the requirements file can be installed with venv.InstallRequirements
func (a *Analyser) EnsureRequirementsFile() (string, error) {
	// per default, requirements.txt should be at top of project directory
	projectDir := path.Join(dir, a.Project.CloneFolder, a.Project.SubFolder)
	reqFile := path.Join(projectDir, "requirements.txt")

	// check whether custom requirements.txt contents were provided
	if a.Project.RequirementsContents != "" {
		a.Log("Custom requirements.txt contents defined!")
		a.Results.HasRequirementsFile = false
		a.Results.CustomRequirements = a.Project.RequirementsContents
		return a.Project.RequirementsFile, a.NewSetupError(ioutil.WriteFile(reqFile, []byte(a.Project.RequirementsContents), 0755))
	}

	// check whether a non-standard location for requirements.txt was provided
	if a.Project.RequirementsFile != "requirements.txt" {
		if utils.FileExists(path.Join(projectDir, a.Project.RequirementsFile)) {
			a.Log("Custom requirements.txt file found in repo!")
			a.Results.HasRequirementsFile = true
			return a.Project.RequirementsFile, nil
		}

		a.LogWarning("A custom requirements file %s was configured, but not found!", a.Project.RequirementsFile)
	}

	// check whether the repo contains a setup.py as a valid alternative to a requirements.txt file
	if utils.FileExists(path.Join(projectDir, "setup.py")) {
		a.Log("Found a setup.py file!")
		a.Results.HasRequirementsFile = true
		return "setup.py", nil
	}

	// generate requirements.txt if necessary
	if !utils.FileExists(reqFile) {
		a.Log("Generating requirements.txt file...")
		err := a.GenerateRequirementsFile()
		a.Results.HasRequirementsFile = false
		a.Results.CustomRequirements = readFile(reqFile)
		return a.Project.RequirementsFile, a.NewSetupError(err)
	}

	// if requirements.txt exists but our marker file also exists,
	// then requirements were generated in a previous run
	if utils.FileExists(path.Join(projectDir, generatedReqsMarkerFile)) {
		a.Log("Generated requirements.txt file found!")
		a.Results.HasRequirementsFile = false
		a.Results.CustomRequirements = readFile(reqFile)
		return a.Project.RequirementsFile, nil
	}

	a.Log("Already has a requirements.txt file")
	a.Results.HasRequirementsFile = true
	return a.Project.RequirementsFile, nil
}

func readFile(filename string) string {
	contents, _ := ioutil.ReadFile(filename)
	return string(contents)
}

// GenerateRequirementsFile generates a requirements file for this project using `pipreqs`
// and creates a markerfile
func (a *Analyser) GenerateRequirementsFile() error {
	projectDir := path.Join(dir, a.Project.CloneFolder, a.Project.SubFolder)

	cmd := exec.Command("pipreqs", ".")
	cmd.Dir = projectDir
	if _, err := cmd.Output(); err != nil {
		return a.NewSetupError(fmt.Errorf("failed to generate requirements file: %w", utils.WrapExitError(err)))
	}

	_, err := os.Create(path.Join(projectDir, generatedReqsMarkerFile))
	if err != nil {
		return a.NewSetupError(fmt.Errorf("failed to create marker file for having generated the requirements: %w", err))
	}

	return nil
}

// CreateVirtualEnv creates a virtualenv in the project folder
func (a *Analyser) CreateVirtualEnv() (*virtualenv.VEnv, error) {
	projectDir := path.Join(dir, a.Project.CloneFolder, a.Project.SubFolder)
	if venv, err := virtualenv.Source(projectDir); err == nil {
		a.Log("Already contains a virtualenv!")
		return venv, nil
	}

	a.Log("Creating a virtualenv...")
	venv, err := virtualenv.Create(projectDir)
	if err != nil {
		return nil, a.NewSetupError(fmt.Errorf("failed to create virtualenv: %w", err))
	}

	return venv, nil
}

// AnalyseProject performs the actual analysis of a project (i.e. a dataset entry)
// It assumes that:
// - the project has already been cloned
// - a requirements file exists, either "requirements.txt" or the one specified manually
// - its requirements have already been pre-installed in the environment (virtualenv) that it is in.
func (a *Analyser) AnalyseProject() error {
	a.Log("Analysing Git stats & counting files...")

	err := a.analyseGitStats()
	if err != nil {
		return a.NewAnalysisError(err)
	}
	err = a.countPythonCode()
	if err != nil {
		return a.NewAnalysisError(err)
	}
	err = a.countIPynbCode()
	if err != nil {
		return a.NewAnalysisError(err)
	}

	a.Log("Found %d .py files (%d lines) and %d .ipynb files (%d lines)", a.Results.NumPythonFiles, a.Results.NumPythonLines, a.Results.NumIPynbFiles, a.Results.NumIPynbLines)
	a.Log("Running pylint...")

	projectDir := path.Join(dir, a.Project.CloneFolder, a.Project.SubFolder)
	lintMessages, err := pylint.AnalyseAll(projectDir)
	if err != nil {
		return a.NewAnalysisError(err)
	}

	a.Results.LintMessages = lintMessages
	a.Results.MessagesPerPath, a.Results.NumMessages = getMetrics(lintMessages)

	return nil
}

func (a *Analyser) analyseGitStats() error {
	repoDir := path.Join(dir, a.Project.CloneFolder)

	commitHash, err := git.GetCurrentCommitHash(repoDir)
	if err != nil {
		return fmt.Errorf("failed to read current Git commit hash: %w", err)
	}
	a.Results.CommitHash = commitHash

	numCommits, err := git.CountCommits(repoDir)
	if err != nil {
		return fmt.Errorf("failed to count number of commits: %w", err)
	}
	a.Results.NumCommits = numCommits

	numContributors, err := git.CountContributors(repoDir)
	if err != nil {
		return fmt.Errorf("failed to count number of contributors: %w", err)
	}
	a.Results.NumContributors = numContributors

	return nil
}

func (a *Analyser) countPythonCode() error {
	projectDir := path.Join(dir, a.Project.CloneFolder, a.Project.SubFolder)

	files, err := utils.FindPythonFilesIn(projectDir)
	if err != nil {
		return fmt.Errorf("error searching for .py files: %w", err)
	}
	files = files.Prefix(projectDir)

	lines, err := utils.CountLinesInFiles(files)
	if err != nil {
		return err
	}

	a.Results.NumPythonFiles = len(files)
	a.Results.NumPythonLines = lines
	return nil
}

func (a *Analyser) countIPynbCode() error {
	projectDir := path.Join(dir, a.Project.CloneFolder, a.Project.SubFolder)

	ipynbFiles, err := utils.FindIPynbFilesIn(projectDir)
	if err != nil {
		return fmt.Errorf("error searching for .py files: %w", err)
	}

	ipynbFiles = ipynbFiles.Prefix(projectDir)
	ipynbLines, err := ipynb.CountTotalLoC(ipynbFiles)
	if err != nil {
		if !errors.Is(err, ipynb.ErrPartialCount) {
			return err
		}
		a.LogWarning("%s", err.Error())
	}

	a.Results.NumIPynbFiles = len(ipynbFiles)
	a.Results.NumIPynbLines = ipynbLines
	return nil
}

func getMetrics(messages []pylint.Message) (map[string]int, map[pylint.MessageType]map[string]int) {
	messagesPerPath := make(map[string]int)
	numMessages := make(map[pylint.MessageType]map[string]int)
	for _, category := range pylint.MessageTypes {
		numMessages[pylint.MessageType(category)] = make(map[string]int)
	}

	for _, message := range messages {
		messagesPerPath[message.Path]++
		messagesPerType := numMessages[message.Type]
		if messagesPerType != nil {
			messagesPerType[message.Symbol]++
		}
	}

	return messagesPerPath, numMessages
}
