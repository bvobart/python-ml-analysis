package csv

import (
	"encoding/csv"
	"fmt"

	"gitlab.com/bvobart/python-ml-analysis/output"
	"gitlab.com/bvobart/python-ml-analysis/pylint"
	"gitlab.com/bvobart/python-ml-analysis/utils"
)

type File [][]string

func NewCSVFile(header []string) File {
	return File{header}
}

func (file File) AddRow(row []string) File {
	return append(file, row)
}

func (file File) WriteFile(filename string) error {
	destination, err := utils.OpenOrCreate(filename)
	if err != nil {
		return fmt.Errorf("failed to open/create results file: %w", err)
	}

	writer := csv.NewWriter(destination)
	err = writer.WriteAll([][]string(file))
	if err != nil {
		return fmt.Errorf("failed to write results to file: %w", err)
	}

	return nil
}

// ------------------------------------------------------------------

var csvHeader = append(append([]string{
	"Project Name",
	"Project URL",
	"Commit Hash",
	"Number of commits",
	"Number of contributors",
	"Number of Python files",
	"Number of IPynb files",
	"Number of lines of pure Python",
	"Number of lines of IPynb Python",
	"Has requirements.txt file",
	"Total Pylint messages"},
	pylint.MessageTypes...),
	pylint.Symbols...,
)

func FromResultsFile(res output.ResultsFile) File {
	csvFile := NewCSVFile(csvHeader)
	for _, results := range res.Results {
		csvFile = csvFile.AddRow(FromResults(results))
	}

	return csvFile
}

func FromResults(res output.Results) []string {
	perMessageType := []string{}
	symbolCount := make(map[string]int)

	for _, category := range pylint.MessageTypes {
		msgsPerType := res.NumMessages[pylint.MessageType(category)]

		// count total number of messages for this type, but also register all symbols seen in separate map.
		total := 0
		for symbol, count := range msgsPerType {
			symbolCount[symbol] = count
			total += count
		}

		perMessageType = append(perMessageType, fmt.Sprint(total))
	}

	perSymbol := []string{}
	for _, symbol := range pylint.Symbols {
		perSymbol = append(perSymbol, fmt.Sprint(symbolCount[symbol]))
	}

	return append(append([]string{
		res.ProjectName,
		res.ProjectURL,
		res.CommitHash,
		fmt.Sprint(res.NumCommits),
		fmt.Sprint(res.NumContributors),
		fmt.Sprint(res.NumPythonFiles),
		fmt.Sprint(res.NumIPynbFiles),
		fmt.Sprint(res.NumPythonLines),
		fmt.Sprint(res.NumIPynbLines),
		fmt.Sprint(res.HasRequirementsFile),
		fmt.Sprint(len(res.LintMessages))},
		perMessageType...),
		perSymbol...,
	)
}
