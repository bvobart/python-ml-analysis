package csv_test

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"

	"gitlab.com/bvobart/python-ml-analysis/output"
	"gitlab.com/bvobart/python-ml-analysis/output/csv"
)

const resultsYML = "test-resources/example-results.yml"
const resultsCSVactual = "test-resources/example-results.actual.csv"
const resultsCSVexpected = "test-resources/example-results.expected.csv"

func before(t *testing.T) {
	require.FileExists(t, resultsYML)
	require.FileExists(t, resultsCSVexpected)
	os.Remove(resultsCSVactual)
}

func prepareResultsFile(t *testing.T) output.ResultsFile {
	rawYml, err := ioutil.ReadFile(resultsYML)
	require.NoError(t, err)

	resultsFile := output.ResultsFile{}
	err = yaml.Unmarshal(rawYml, &resultsFile)
	require.NoError(t, err)

	return resultsFile
}

func TestResultsToCSV(t *testing.T) {
	before(t)
	resultsFile := prepareResultsFile(t)

	err := csv.FromResultsFile(resultsFile).WriteFile(resultsCSVactual)
	require.NoError(t, err)
	require.FileExists(t, resultsCSVactual)

	actual, err := ioutil.ReadFile(resultsCSVactual)
	require.NoError(t, err)
	expected, err := ioutil.ReadFile(resultsCSVexpected)
	require.NoError(t, err)
	require.Equal(t, string(expected), string(actual))
}
