package pylint_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bvobart/python-ml-analysis/pylint"
)

func TestMessageListByType(t *testing.T) {
	list := pylint.MessageList(exampleMessages)
	result := list.ByType("warning")
	require.NotNil(t, result)
	require.Equal(t, pylint.MessageList{exampleMessages[0], exampleMessages[6]}, result)
}
