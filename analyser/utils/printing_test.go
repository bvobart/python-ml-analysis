package utils

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFixedLength(t *testing.T) {
	inputs := map[string]string{
		"short":                                  "short                           ",
		"superVeryExtremesuperVeryExtremelyLong": "superVeryExtremesuperVeryExtreme",
		"jemoederiseenplopkoeklolwtfomfgbbq":     "jemoederiseenplopkoeklolwtfomfgb",
		"12345678901234567890123456789012":       "12345678901234567890123456789012",
	}

	for input, expected := range inputs {
		output := FixedLength(input)
		require.Equal(t, expected, output)
	}
}
