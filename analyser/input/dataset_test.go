package input_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bvobart/python-ml-analysis/input"
)

var expected = input.Dataset{
	[]input.Project{
		{ID: 1, Name: "sma2c-ipd", CloneURL: "https://github.com/Pluriscient/sma2c-ipd.git", CloneFolder: "sma2c-ipd", RequirementsFile: "somefolder/requirements.txt"},
		{ID: 2, Name: "DL-unet", CloneURL: "https://github.com/nsirons/DL-unet.git", CloneFolder: "DL-unet", RequirementsFile: "requirements.txt", RequirementsContents: "torch==1.7.0\nnumpy==1.19.4\nfvcore==0.1.2.post20201204\n"},
		{ID: 3, Name: "Deformable-DETR", CloneURL: "https://github.com/fundamentalvision/Deformable-DETR.git", CloneFolder: "Deformable-DETR", RequirementsFile: "requirements.txt"},
		{ID: 4, Name: "navigan", CloneURL: "https://github.com/yandex-research/navigan.git", CloneFolder: "navigan", RequirementsFile: "requirements.txt"},
		{
			ID:                   5,
			Name:                 "PrediNet (deepmind-research)",
			CloneURL:             "https://github.com/deepmind/deepmind-research.git",
			CloneFolder:          "deepmind-research",
			RequirementsFile:     "requirements.txt",
			RequirementsContents: "torch==1.7.0\nnumpy==1.19.4\n",
			SubFolder:            "PrediNet",
		},
		{
			ID:               6,
			Name:             "affordances_theory (deepmind-research)",
			CloneURL:         "https://github.com/deepmind/deepmind-research.git",
			CloneFolder:      "deepmind-research",
			RequirementsFile: "somefolder/requirements.txt",
			SubFolder:        "affordances_theory",
		},
		{
			ID:                7,
			Name:              "alphafold_casp13 (deepmind-research)",
			CloneURL:          "https://github.com/deepmind/deepmind-research.git",
			CloneFolder:       "deepmind-research",
			RequirementsFile:  "requirements.txt",
			SubFolder:         "alphafold_casp13",
			ExtraRequirements: []string{"torch==1.7.0"},
		},
		{
			ID:               8,
			Name:             "byol (deepmind-research)",
			CloneURL:         "https://github.com/deepmind/deepmind-research.git",
			CloneFolder:      "deepmind-research",
			RequirementsFile: "requirements.txt",
			SubFolder:        "byol",
		},
		{
			ID:                   9,
			Name:                 "PanopticFCN",
			CloneURL:             "https://github.com/yanwei-li/PanopticFCN.git",
			CloneFolder:          "PanopticFCN",
			RequirementsFile:     "requirements.txt",
			RequirementsContents: "torch==1.7.1\nnumpy==1.19.4\nfvcore==0.1.2.post20201204\n",
			SubFolder:            "",
			ExtraRequirements:    []string{"git+https://github.com/facebookresearch/detectron2.git@v0.3#egg=detectron2"},
		},
	},
}

func TestReadDataset(t *testing.T) {
	const datasetFile = "test-resources/test-dataset.yml"
	dataset, err := input.ReadDataset(datasetFile)
	require.NoError(t, err)
	require.Equal(t, &expected, dataset)
}

func TestReadNoUrl(t *testing.T) {
	const datasetFile = "test-resources/no-url-dataset.yml"
	_, err := input.ReadDataset(datasetFile)
	require.Error(t, err)
	require.Equal(t, "project 1 -  is invalid: no `url` provided to use with `git clone`", err.Error())
}

func TestReadNoFile(t *testing.T) {
	const datasetFile = "test-resources/does-not-exist.yml"
	_, err := input.ReadDataset(datasetFile)
	require.Error(t, err)
	require.Equal(t, "open test-resources/does-not-exist.yml: no such file or directory", err.Error())
}
