package virtualenv

import (
	"errors"
	"fmt"
	"os/exec"
	"path"
	"strings"

	"gitlab.com/bvobart/python-ml-analysis/utils"
)

var (
	ErrNoVenvDir      = errors.New(".venv does not exist, create a virtualenv in this directory first")
	ErrNoVenvActivate = errors.New(".venv exists, but .venv/bin/activate does not")
)

// VEnv represents an activated virtualenv
type VEnv struct {
	dir string
}

// Create creates a virtualenv in directory $dir/.venv
// Uses --system-site-packages to avoid disk space explosion with a load of projects.
func Create(dir string) (*VEnv, error) {
	cmd := exec.Command("virtualenv", ".venv", "--system-site-packages")
	cmd.Dir = dir
	if _, err := cmd.Output(); err != nil {
		return nil, utils.WrapExitError(err)
	}
	return &VEnv{dir}, nil
}

// Source creates an instance of a VEnv struct by checking whether the dir is a virtualenv directory.
func Source(dir string) (*VEnv, error) {
	if err := Check(dir); err != nil {
		return nil, err
	}
	return &VEnv{dir}, nil
}

// Is tests whether the given directory contains a virtualenv
func Is(dir string) bool {
	err := Check(dir)
	return err == nil
}

// Check checks whether the given directory contains a virtualenv and returns an error if it is not and why.
func Check(dir string) error {
	if !utils.DirExists(path.Join(dir, ".venv")) {
		return fmt.Errorf("%s/%w", dir, ErrNoVenvDir)
	}
	if !utils.FileExists(path.Join(dir, ".venv/bin/activate")) {
		return fmt.Errorf("%s/%w", dir, ErrNoVenvActivate)
	}
	return nil
}

// Command runs the given Bash command after sourcing the virtualenv's activate script.
func (env *VEnv) Command(command string) *exec.Cmd {
	cmd := exec.Command("bash", "-c", "source .venv/bin/activate && "+command)
	cmd.Dir = env.dir
	return cmd
}

// GenerateRequirements uses `pipreqs` in this virtualenv to generate a requirements file from
// the modules that are imported in the Python files.
func (env *VEnv) GenerateRequirements() error {
	cmd := env.Command("pipreqs .")
	if _, err := cmd.Output(); err != nil {
		return utils.WrapExitError(err)
	}
	return nil
}

func (env *VEnv) InstallRequirement(req string) error {
	cmd := env.Command("pip install " + req)
	if _, err := cmd.Output(); err != nil {
		return utils.WrapExitError(err)
	}
	return nil
}

// InstallRequirements uses `pip install -r file` in this virtualenv to install all requirements
// from the given requirements file, or `pip install -e .` when the file is a setup.py file.
func (env *VEnv) InstallRequirementsFile(file string) error {
	cmd := env.installRequirementsFileCommand(file)
	if _, err := cmd.Output(); err != nil {
		return utils.WrapExitError(err)
	}
	return nil
}

func (env *VEnv) installRequirementsFileCommand(file string) *exec.Cmd {
	if isSetupPy(file) {
		return env.Command("pip install -e .")
	}
	return env.Command("pip install -r " + file)
}

func isSetupPy(file string) bool {
	return strings.HasSuffix(strings.TrimSpace(file), "setup.py")
}
