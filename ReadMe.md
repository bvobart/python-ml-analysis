# Static analysis on Python ML Code with Pylint
[![pipeline status](https://gitlab.com/bvobart/python-ml-analysis/badges/master/pipeline.svg)](https://gitlab.com/bvobart/python-ml-analysis/-/commits/master)

This is a tool to perform simultaneous static analysis using Pylint on a dataset of Machine Learning (ML) and Artificial Intelligence (AI) projects written in Python. The tool itself is written in Go, as it compiles quickly, runs fast and is extremely well suited for concurrency so that multiple projects can be analysed at the same time (as much projects as there are threads available on your device).

This tool is being developed as part of my MSc thesis at the [Software Engineering Research Group (SERG)](https://se.ewi.tudelft.nl/) of the [Delft University of Technology](https://www.tudelft.nl/) in collaboration with [ING](https://www.ing.com/)'s [AI for Fintech Research Lab](https://icai.ai/ai-for-fintech-lab/). The goal of my thesis is to investigate and improve the state of code quality practices in ML and AI, as well as to investigate code smells specific to ML and AI.

## How do I run this tool?

To run this tool, all you need is a Linux shell and Docker. Then run the following two `bash` scripts to respectively build and run the Docker container that will perform the analysis:

```sh
./build.sh
./run.sh
```

Any arguments passed to `run.sh` will be passed to `docker run` after the specification of the image. This means that `./run.sh bash` will run a `bash` session inside of the container instead of the usual command to run the analyser (`./analyser`).

## How does it work?

The analysis tool performs the following actions:

1. Parses `./data/dataset.yml`, checks it for validity (i.e. no missing URLs), splits projects with folders in them to multiple projects.

2. Then, for each project, in parallel:
    1. Clone the project using the url of the Git repository (HTTPS only, SSH clone is not working as of yet)
    2. Ensure the existance of a file that specifies any dependencies / requirements. This is fulfilled when...
        - ... there's a `requirements.txt` at the project's root directory, as per convention.
        - ... there's a `setup.py` at the project's root directory, as per convention.
        - ... the project's definition in the dataset specificies `requirements: ...` with a relative file path to a custom requirements file in some non-standard location.
        - ... the project's definition in the dataset specificies `requirements.txt: |` with a multi-line string specifying contents of a `requirements.txt` file. This will overwrite any `requirements.txt` file currently the top-level of the repo.
        - ... none of the above apply, but a `requirements.txt` file can be generated using `pipreqs`.
    3. Create a virtualenv with `--system-site-packages`
    4. Install the requirements from the requirements file into the virtualenv.
    5. Perform the static code analysis:
        1. Take note of some Git statistics, e.g. the current commit hash on the repo, number of commits and number of contributors.
        2. Count how many Python files and lines of Python there are in the project's repository, as well as how many Jupyter Notebooks there are and how many lines of Python are embedded in these notebooks.
        3. Run [`pylint`](https://www.pylint.org/) on all Python files in the project
        4. Capture and parse `pylint`'s JSON output

3. Wait for every project to finish
4. Collect the output from each project, then save this to a humanly insightful YAML file
5. Save the collected output to a CSV file as well for further statistical analysis.


### Caching

To speed up the analysis when running this tool multiple times sequentially, the `run.sh` script automatically create a few folders in the current directory which are mounted to the analyser's container as volumes. These have the following names and purposes:

- `.cache` - for storing `pip`'s download cache. Dependencies should therefore only need to be downloaded once.
- `projects` - for storing the projects' files, so that the projects don't have to be cloned again every time the analysis runs.
- `results` - not really used as cache, but is created automatically to save the resulting YAML and CSV files.

> Note: why folders instead of Docker volumes? Upon creation, Docker volumes are owned by the `root` user, but the analyser's container runs as the same user as the host (which is almost never `root`). In short, that saves a whole load of issues with file permissions.

## Dataset of projects

The `dataset.yml` file in the `data` folder in this repository contains the currently defined dataset of projects which this tool will analyse. We aim for this dataset to be a systematically gathered set of projects, representative of the current, real-world state of ML and AI projects. To this end, we have set up a set of guidelines for the inclusion of projects in the dataset, which can be found below.

> Note: not all projects defined in the dataset can be analysed successfully. Some projects still have some problems making the analysis fail with an error. These projects have been skipped and do not count towards the total amount of projects in our dataset. All projects that have not been skipped do work.

### Guidelines for adding a project to the dataset

Each project in the dataset must adhere to the following guidelines:
- The project must be hosted in an open-source Git repository.
- The project must be written in Python 3.
- The project must contain pure Python code, i.e. code in `.py` files and must not consist purely of Jupyter Notebooks for reasons explained in the [Limitations](#pure-python-vs-jupyter-notebooks) section. More specifically, a project should contain:
    - **Either:** at least 200 lines of pure Python code, even if the rest is in Jupyter Notebooks.
    - **Or:** more lines of pure Python code than lines of Python embedded in all Jupyter Notebooks of that project.
- The project must be an application of ML / AI, i.e., it must implement an ML or AI model and may not be a library or tool for use in ML projects.
- The project must be considered 'deliverable', i.e.:
    - the project is part of or accompanies a published academic paper.
    - the project has been submitted to [reproducedpapers.org](https://reproducedpapers.org), [paperswithcode.com](https://paperswithcode.com) or a finished Kaggle competition.

Please let us know if you see a way to improve these guidelines :)

#### Deliverability

The last guideline in the list above states that we want 'deliverable' projects. This is quite difficult to define, but we consider a project deliverable when either of the two conditions apply. The guideline mainly embodies that we are not interested in toy projects, unfinished projects or projects still under development. 

## Limitations

There are a few limitations to this tool, as explained below.

### Pure Python vs. Jupyter Notebooks

This tool only analyses pure Python (`.py`) files as that is what is currently supported by Pylint. However, many data science projects (also) make use of Jupyter Notebooks (`.ipynb`) which Pylint cannot check directly. It could be possible to first convert the notebook into Python files, but some of Pylint's rules wouldn't make sense on the extracted Python code, see [this StackOverflow post](https://stackoverflow.com/questions/50358327/using-pylint-in-ipython-jupyter-notebook).

### Pylint's RecursionError bug

At the moment of writing, there is [a bug](https://github.com/PyCQA/pylint/issues/3986) in Pylint where Pylint crashes on any project that imports `seaborn` or uses `merge` from `pandas`. Since these libraries are heavily used in ML and AI, there are quite some projects that fail the analysis, reporting the following in the error output: `error parsing Pylint output: invalid character 'T' looking for beginning of value`
